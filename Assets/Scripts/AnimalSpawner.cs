﻿using UnityEngine;
using System.Collections;

public class AnimalSpawner : MonoBehaviour {
	
	public GameObject one;
	
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	
		if(Input.GetKeyDown(KeyCode.Alpha1))
			spawn ();
	}
	
	void spawn(){
		
		for(int i = 0 ; i < 10 ; i++)
			Instantiate( one, new Vector3( Random.Range ( -3.0f, 3.0f ), Random.Range ( -3.0f, 3.0f ), 0.0f ), Quaternion.Euler ( 0.0f, 0.0f, 0.0f ) );
	}
}
