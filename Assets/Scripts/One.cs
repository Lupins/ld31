﻿using UnityEngine;
using System.Collections;

public class One : MonoBehaviour {

	public GameObject two;
	
	private int life;
	private bool reproduce;

	// Use this for initialization
	void Start () {
	
		setReproduce(false);
		setLife (5);

		wait (5);
		
		setReproduce(true);
	}
	
	// Update is called once per frame
	void Update () {

		if( getLife () <= 0 )
			Destroy (this.gameObject);

		Vector3 pos = transform.position;
		
		//50% of Walk
		if( Random.Range (0.0f, 1.0f) >= 0.5f ){
		
			//50% of Vertical
			if( Random.Range (0.0f, 1.0f) >= 0.5f ){
				//Inside Screen
				if( pos.y <= 4.7f && pos.y >= -4.7f )
					//50% of Positive
				    if( Random.Range ( 0.0f, 1.0f ) >= 0.5f ){
					    pos.y += 0.1f;
					//50% of Negative
				    }else{
					    pos.y -= 0.1f;
				    }
			//50% Horizontal
			}else{
				//Inside Screen
				if( pos.x <= 5.7f && pos.x >= -5.7f )
					//50% of Positive
					if( Random.Range ( 0.0f, 1.0f ) >= 0.5f ){
						pos.x += 0.1f;
					//50% of Negative
				    }else{
				        pos.x -= 0.1f;
				    }
		    }
				
			//Perform movement
			transform.position = pos;
		}
	}
	
	void OnTriggerEnter(Collider other){
		
		if(other.CompareTag("One")){
			
			//50% of Reproduce
			if( Random.Range (0.0f, 1.0f) <= 0.05f && getReproduce ()){
			
				setReproduce (false);
				//25% of Two
				if( Random.Range (0.0f, 1.0f) <= 0.25f){
					spawnCreature(1);
				//75% of One
				}else
					spawnCreature(0);

				//Lose life
				setLife ( getLife() - 1 );
				
				//Wait after reproduction
				wait (5);
				setReproduce(true);
			}
		//Died
		}else if( other.CompareTag("Two"))
			Destroy (this.gameObject);
	}

	void spawnCreature(int ID){

		switch(ID){
			case 0:
			    Instantiate (this.gameObject, transform.position, Quaternion.Euler (0.0f, 0.0f, 0.0f) );
				break;

			case 1:
			    Instantiate (two, transform.position, Quaternion.Euler ( 0.0f, 0.0f, 0.0f ) );
				break;
		}
	}

	IEnumerator wait(int value){
		
		yield return new WaitForSeconds(value);
	}
		
	void setReproduce(bool value){
		reproduce = value;
	}
	
	void setLife(int value){
		life = value;
	}
	
	bool getReproduce(){
		return reproduce;
	}
	
	int getLife(){
		return life;
	}
}
