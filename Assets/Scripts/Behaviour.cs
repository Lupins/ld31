﻿using UnityEngine;
using System.Collections;

public class Behaviour : MonoBehaviour {

	// Use this for initialization
	void Start () {
		transform.rotation = Quaternion.Euler( 90.0f, 0.0f, 0.0f );
	}
	
	// Update is called once per frame
	void Update () {
		
		Vector3 pos = transform.position;
		if( Random.Range (0.0f, 1.0f) >= 0.5f ){
		
			if( Random.Range (0.0f, 1.0f) >= 0.5f ){
				if( pos.z <= 4.7f || pos.z >= -4.7f )
					if( Random.Range ( 0.0f, 1.0f ) >= 0.5f )
						pos.z += 0.1f;
				    else
					    pos.z -= 0.1f;
			}else{
				if( pos.x <= 5.7f || pos.x >= -5.7f )
					if( Random.Range ( 0.0f, 1.0f ) >= 0.5f )
						pos.x += 0.1f;
				    else
				        pos.x -= 0.1f;
		    }
				
			transform.position = pos;
		}
	}
	
	IEnumerator timer(){
		yield return new WaitForSeconds(1);
	}
	
	void OnTriggerEnter2D(Collider2D other){
		
		if(other.CompareTag("One")){
			if( Random.Range (0.0f, 1.0f) <= 0.05f ){
				print ("Born!");
				AnimalSpawner.spawnAnimal(0, transform);
			}
		}
	}
}
