﻿using UnityEngine;
using System.Collections;

public class Two : MonoBehaviour {
	
	private int timeToDie;
	// Use this for initialization
	void Start () {
	
		timeToDie = 5;
	}
	
	// Update is called once per frame
	void Update () {
	
		Vector3 pos = transform.position;
		
		//50% of Walk
		if( Random.Range (0.0f, 1.0f) >= 0.5f ){
			
			//50% of Vertical
			if( Random.Range (0.0f, 1.0f) >= 0.5f ){
				//Inside Screen
				if( pos.y <= 4.7f && pos.y >= -4.7f )
					//50% of Positive
				if( Random.Range ( 0.0f, 1.0f ) >= 0.5f ){
					pos.y += 0.1f;
					//50% of Negative
				}else{
					pos.y -= 0.1f;
				}
				//50% Horizontal
			}else{
				//Inside Screen
				if( pos.x <= 5.7f && pos.x >= -5.7f )
					//50% of Positive
				if( Random.Range ( 0.0f, 1.0f ) >= 0.5f ){
					pos.x += 0.1f;
					//50% of Negative
				}else{
					pos.x -= 0.1f;
				}
			}
			
			//Perform movement
			transform.position = pos;
		}
	}
	
	void OnTriggerEnter( Collider other ){
		
		//50% of fight and die
		if( other.CompareTag("Two") && Random.Range (0.0f, 1.0f) <= 0.5f )
			Destroy (this.gameObject);
		else if( other.CompareTag("One"))
			timeToDie--;
			
		if(timeToDie <= 0)
			Destroy (this.gameObject);
	}
	
	IEnumerator wait(int value){
	
		yield return new WaitForSeconds(value);
	}
}
